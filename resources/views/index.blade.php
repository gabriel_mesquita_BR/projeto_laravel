@extends('template')

@section('conteudo')

		<div class="super_container">
			
			<!-- Header -->

			<header class="header">
				
				<div class="container">
									
					<div class="titulo">The Place</div>
					<div class="subtitulo">restaurant</div>
									
				<div>

			</header>

			<!-- Home -->

			<div class="home">
				<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="/images/home.jpg" data-speed="0.8"></div>
				<div class="home_container">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="home_content text-center">
									<div class="home_subtitle page_subtitle">The Place é</div>
									<div class="home_title"><h1>Uma Experiência Extraordinária</h1></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="scroll_icon"></div>

				<div>

					@if(Auth::check())

						<a href="/logout" class="btn btn-danger botao-logout">Logout</a>

						<a href="/tarefas/lista" class="btn btn-info botao-visualizar">Visualizar</a>

					@endif

					<a href="/home" class="btn btn-primary botao-login">Login</a>
				
				</div>

			</div>

			<!-- Intro -->

			<div class="intro">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="row">
								<div class="col-xl-4 col-md-6 intro_col">
									<div class="intro_image"><img src="/images/intro_1.jpg"></div>
								</div>
								<div class="col-xl-4 col-md-6 intro_col">
									<div class="intro_image"><img src="/images/intro_2.jpg"></div>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>

			<!-- Reservations -->

			<div class="reservations text-center">
				<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="/images/reservations.jpg" data-speed="0.8"></div>
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="reservations_content d-flex flex-column align-items-center justify-content-center">
								<div class="res_stars page_subtitle">5 Stars</div>
								<div class="res_form_container"></div>
							</div>
						</div>
					</div>
				</div>		
			</div>

			<!-- Footer -->

			<footer class="footer">
				<div class="container">
					<div class="row">

						<!-- Footer Logo -->
						<div class="col-lg-3 footer_col">
							<div class="footer_logo">
								<div class="footer_logo_title">The Place</div>
								<div class="footer_logo_subtitle">restaurant</div>
							</div>
							<div class="copyright"><!-- Link back to Colorlib can't be removed. Template is  
								                        licensed under CC BY 3.0. -->
								
								<p style="line-height: 1.2;">Copyright &copy;
									<script>
										document.write(new Date().getFullYear());
									</script>

									All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
							    </p>
								<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0.-->
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>

		<script src="/js/jquery.js"></script>
		<script src="/js/parallax.min.js"></script>

@stop