@extends('template')

@section('conteudo')

	<div id="erro">
		<div class="erro">
			<div class="erro-status">
				<h1>Oops!</h1>
			</div>
			<h2>404 - Página Não Encontrada</h2>
			<a href="/">VOLTAR PRA HOME</a>
		</div>
	</div>

	<!-- This templates was made by Colorlib (https://colorlib.com) -->

@stop