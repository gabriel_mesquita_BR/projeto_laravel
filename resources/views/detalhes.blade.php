@extends('template')

@section('conteudo')
	
	<h2> Detalhes da tarefa: {{ $tarefa->nome }} </h2>

	<uL class="list-group">
		<li class="list-group-item active"> Descrição: {{ $tarefa->descricao }} </li>
		<li class="list-group-item active"> Prazo: {{ $tarefa->prazo }} </li>
		<li class="list-group-item active"> Prioridade: {{ $tarefa->prioridade->ordem }} </li>
		<li class="list-group-item active"> Concluída: {{ $tarefa->concluida->resposta }}</li>
	</ul>

@stop