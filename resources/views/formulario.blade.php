@extends('template')

@section('conteudo')
	
	@if($errors->all())

		<div class="alert alert-danger">

			<ul>

				@foreach($errors->all() as $error)

					<li> {{$error}} </li>

				@endforeach
			</ul>

		</div>
		
	@endif

	<form class="formulario" action="/tarefas/adiciona" method="post">

		<input type="hidden" name="_token" value="{{ csrf_token() }}"/>

		<div class="form-group">
			<label for="nome">Nome</label>
			<input id="nome" name="nome" class="form-control"/>
		</div>

		<div class="form-group">
			<label for="descricao">Descrição</label>
			<textarea id="descricao" name="descricao" class="form-control" rows="5"></textarea>
		</div>

		<div class="form-group">
    		<label for="prazo">Prazo</label>
    		<div class="input-group date data_formato" data-date-format="dd-mm-yyyy HH:ii:ss">
    			<input type="text" class="form-control" id="prazo" name="prazo"/>
    			<span class="input-group-addon">
    				<span class="glyphicon glyphicon-th"></span>
    			</span>
    		</div>
    	</div>
    
		<div class="form-group">
			<label for="prioridade">Prioridade</label>
			<select id="prioridade" name="prioridade_id" class="form-control">

				@foreach($prioridades as $prioridade)
					<option value="{{ $prioridade->id }}">{{ $prioridade->ordem }}</option>
				@endforeach
			
			</select>
		</div>

		<div class="form-group">
			<label for="concluida">Concluída</label>
			<select id="concluida" name="concluida_id" class="form-control">

				@foreach($concluidas as $concluida)
					<option value="{{ $concluida->id }}">{{ $concluida->resposta }}</option>
				@endforeach
			
			</select>
		</div>

		<button class="btn btn-success" type="submit">Adicionar</button>

	</form>

	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/js/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript" src="/js/locales/bootstrap-datetimepicker.pt-BR.js"></script>
	<script type="text/javascript" src="/js/datetimepicker.js"></script>

@stop