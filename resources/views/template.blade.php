<!DOCTYPE html>
<html>
<head>
	<title>Controle de tarefas</title>
	<link rel="stylesheet" type="text/css" href="/css/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" type="text/css" href="/css/estilos.css">
	<link rel="stylesheet" type="text/css" href="/css/app.css">
	<link type="text/css" rel="stylesheet" href="/css/estilo-erro.css">
</head>
<body>

	@yield('conteudo')

</body>
</html>