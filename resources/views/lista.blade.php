@extends('template')

@section('conteudo')
	
	<h2 class ="titulo-lista">Listagem De Tarefas</h2>

	<a href="/" class="btn botao-home ">Home</a>
	<a href="/tarefas/lista" class="btn botao-lista">Lista Sem Filtros</a>

	<form action="/tarefas/lista-filtrada" method="post">

    	<input type="hidden" name="_token" value="{{ csrf_token() }}">

    	<div class="form-group filtro">
			
			<select id="prioridade" name="prioridade_id" class="select-filtro-prioridade">

				<option value="">-- Prioridade --</option>

				@foreach($prioridades as $prioridade)
					<option value="{{ $prioridade->id }}">{{ $prioridade->ordem }}</option>
				@endforeach
			
			</select>

			<select id="concluida" name="concluida_id" class="select-filtro-concluida">

				<option value="">-- Concluída --</option>

				@foreach($concluidas as $concluida)
					<option value="{{ $concluida->id }}">{{ $concluida->resposta }}</option>
				@endforeach
			
			</select>

			<button class="btn btn-primary botao-filtro" type="submit">Pesquisar</button>

		</div>

	</form>

	<table class="table table-striped">
			
		<thead>
			<tr>
				<th>Tarefa</th>
				<th>Descrição</th>
				<th>Prazo</th>
				<th>Prioridade</th>
				<th>Concluída</th>
			</tr>
		</thead>

		@foreach($tarefas as $tarefa)

			<tr class="{{ $tarefa->prioridade->ordem == 'Muito Alta' ? 'danger' : '' }}">

				<td> {{ $tarefa->nome }} </td>
				<td> {{ substr($tarefa->descricao, 0, 20) }} </td>
				<td> {{ $tarefa->prazo }} </td>
				<td> {{ $tarefa->prioridade->ordem }} </td>
				<td> {{ $tarefa->concluida->resposta }} </td>	
			
				<td>	
					<a href="/tarefas/novo" class="btn btn-primary">
						<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
					</a>
				</td>

				<td>	
					<a href="/tarefas/mostra/{{ $tarefa->id }}"" class="btn btn-info">
						<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
					</a>
				</td>

				<td>	
					<a href="/tarefas/edita/{{ $tarefa->id }}" class="btn btn-default">
						<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
					</a>
				</td>

				<form action="/tarefas/remove" method="post">

					<td>
		       			
		       			<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="tarefa_id" value="{{ $tarefa->id }}">

						<button type="submit" class="btn btn-danger">
							<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
						</button>

					</td>

				</form>
				
			</tr>			

		@endforeach

	</table>

	@if (isset($dados))

		{!! $tarefas->appends($dados)->render() !!}

	@else

		{!! $tarefas->render() !!}

	@endif

	@if(old('nome'))
	
		<div class="alert alert-success">
			Tarefa {{ old('nome') }} adicionada com sucesso!
		</div>

	@endif

	@if(Session::has('mensagem'))
		
		<div class="alert alert-success">
			{{ Session::get('mensagem') }}
		</div>
	
	@endif
	
@stop