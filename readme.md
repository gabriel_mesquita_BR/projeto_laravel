##Projeto construído usando laravel 5.0 via Windows

Para executarmos o nosso projeto é necessário as ferramentas básicas para desenvolvimento em php, sugere-se o 
WAMP (Windows), LAMP (Linux) ou XAMPP (Multiplataforma), pois já incluem o servidor Apache, banco de dados MySQL
e o próprio Php.
O composer pode ser instalado no Windows via executável ou no linux através dos comandos:

curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

Agora iremos instalar o Laravel mostrando 2 maneiras diferentes, sendo o comando o mesmo tanto para Windows como
para Linux:

composer global require "laravel/installer=~1.1"

Outra maneira seria instalar o Laravel via composer create-project especificando a versão que no nosso caso foi a
5.0:

composer create-project laravel/laravel nome_do_projeto "5.0"

Após feito tudo isso é necessário ter o seguinte caminho adicionado no seu PATH:

Windows: C:\Users\gabriel\AppData\Roaming\Composer\vendor\bin
Linux: export PATH="~/.composer/vendor/bin:$PATH"

Após isso no Windows já pode utilizar o Laravel, no Linux antes temos que instalar a extensão mcrypt:

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install php5-json
sudo apt-get install openssl
sudo apt-get install php5-mcrypt
sudo php5enmod mcrypt

Agora já pode utilizar o Laravel no Linux.

##Banco de dados usado: MySQL

No arquivo config\database.php definimos o banco de dados, host, database, username, password.
No arquivo .env definimos o host, database, username, password.
Obs: É necessário o database já estar criado no banco de dados antes de levantar o projeto e para adequar-se à sua
máquina é possível fazer alterações nos arquivos config\database.php e .env, onde serão definidos o seu database,
banco de dados, username, password, host, para que a integração com o banco de dados funcione corretamente.

##Após fazer as configurações relacionadas ao banco de dados executar os seguintes comandos no terminal dentro do
##diretório do projeto para que as tabelas, os relacionamentos e os dados padrões sejam criados no seu database

Para que as tabelas e os relacionamentos sejam criadas no seu database digite no terminal: php artisan migrate
Para que os dados padrões sejam criados nas tabelas digite no terminal: php artisan db:seed

##Levantando o projeto após baixá-lo do BitBucket, extraí-lo e ter feito as configurações relacionadas ao banco de
##dados

Após baixar o projeto do BitBucket e extraí-lo, no terminal dentro do diretório do projeto chamado:
gabriel_mesquita_BR-projeto_laravel-aqui_vai_estar_um_código digitamos o seguinte comando:

php artisan serve

Por fim colocamos a uri gerada no browser, sendo que para os comandos do artisan funcionar como o utilizado acima
para levantar a nossa aplicação é necessário estar dentro da pasta do projeto no terminal.

##Branchs

Criamos outros branchs para simularmos um ambiente de desenvolvimento com várias pessoas trabalhando no mesmo 
projeto, evitando possíveis conflitos de arquivos que poderiam surgir caso mais de uma pessoa estivesse trabalhando
na mesma branch.

##Restlet Client

Para os testes no nosso sistema usando o web service Rest que usa Json foi utilizado o HttpClient chamado Restlet
Client.