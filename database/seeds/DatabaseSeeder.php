<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Prioridade;
use App\Concluida;
use App\Tarefa;

class DatabaseSeeder extends Seeder {

	public function run() {

		Model::unguard();

		$this->call('PrioridadeTableSeeder');
		$this->call('ConcluidaTableSeeder');
		$this->call('TarefaTableSeeder');
	}
}

class PrioridadeTableSeeder extends Seeder {

	public function run() {

		Prioridade::create(['ordem' => 'Muito Alta']);
		Prioridade::create(['ordem' => 'Alta']);
		Prioridade::create(['ordem' => 'Média']);
		Prioridade::create(['ordem' => 'Baixa']);
	}
}

class ConcluidaTableSeeder extends Seeder {

	public function run() {

		Concluida::create(['resposta' => 'Sim']);
		Concluida::create(['resposta' => 'Não']);
	}
}

class TarefaTableSeeder extends Seeder {

	public function run() {

		Tarefa::create(['nome' => 'Preparar o almoço', 'descricao' => 'O almoço será frango, arroz, feijão, batata',
			            'prazo' => '2018-10-21 12:00:00', 'prioridade_id' => '2', 'concluida_id' => '1']);

		Tarefa::create(['nome' => 'Preparar a merenda', 'descricao' => 'A merenda será x burguer, salgado,suco',
			            'prazo' => '2018-10-21 15:00:00', 'prioridade_id' => '3', 'concluida_id' => '1']);

		Tarefa::create(['nome' => 'Preparar o jantar', 'descricao' =>
			            'O jantar será carne seca acebolada com manteiga de garrafa', 'prazo' =>
			            '2018-10-21 18:00:00', 'prioridade_id' => '2', 'concluida_id' => '2']);

		Tarefa::create(['nome' => 'Pedir ao fornecedor para trazer a nova remessa de alimentos e bebidas do mês', 
			            'descricao' => 'Remessa de alimentos e bebidas do mês', 'prazo' => '2018-12-22 17:00:00', 
			            'prioridade_id' => '1', 'concluida_id' => '2']);

		Tarefa::create(['nome' => 'Preparar a sobremesa', 'descricao' => 'Brownie com sorvete', 
			            'prazo' => '2018-12-22 18:15:00', 'prioridade_id' => '2', 'concluida_id' => '2']);
	}
}