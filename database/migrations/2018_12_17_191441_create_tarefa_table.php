<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\BD;

class CreateTarefaTable extends Migration {

	public function up() {

		Schema::create('tarefas', function(Blueprint $table) {

			$table->increments('id');
			$table->string('nome');	
			$table->string('descricao');
			$table->datetime('prazo');
			
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}

	public function down() {

		Schema::drop('tarefa');
	}
}