<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\BD;

class AdicionaRelacionamentoTarefaConcluida extends Migration {

	public function up() {

		Schema::table('tarefas', function(Blueprint $table) {

			$table->integer('concluida_id')->default(1);
		});	
	}

	public function down() {

		Schema::table('tarefas', function(Blueprint $table) {

			$table->dropColumn('concluida_id');

		});
	}
}