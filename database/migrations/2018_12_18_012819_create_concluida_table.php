<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\BD;

class CreateConcluidaTable extends Migration {

	public function up() {

		Schema::create('concluidas', function(Blueprint $table) {

			$table->increments('id');
			$table->string('resposta');

			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
		
	}

	public function down() {

		Schema::table('concluidas', function(Blueprint $table) {

			$table->drop('concluidas');
		});
	}
}