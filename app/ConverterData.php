<?php namespace App;

use Illuminate\Database\Eloquent\Model;

//regra de negócio
class ConverterData extends Model {

	public static function data_formato_brasileiro_para_formato_bd($data_hora) {

		$data = explode(" ", $data_hora);
		list($date, $hora) = $data;

		//verifica se a data está no formato brasileiro
		if (preg_match("/\d{2}-\d{2}-\d{4}/", $date)) {

			$data_convertida = array_reverse(explode("-", $date));
			$data_convertida = implode("-", $data_convertida);
			$data_hora = $data_convertida . " " . $hora;		
		}

		return $data_hora;
	}
}