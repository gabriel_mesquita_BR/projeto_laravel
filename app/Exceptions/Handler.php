<?php namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Handler extends ExceptionHandler {

	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		'Symfony\Component\HttpKernel\Exception\HttpException'
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $e
	 * @return void
	 */
	public function report(Exception $e)
	{
		return parent::report($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $e
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e) {
		
		if ($this->isHttpException($e)) {

			switch($e->getStatusCode()) {

				case 404:

					//return response()->json([ 'mensagem' => 'Tarefa não encontrada' ], 404);
					return response()->view('erro404', [], 404);
					break;

				case 405:

					//return response()->json([ 'mensagem' => 'Acesso não autorizado' ], 405);
					return response()->view('erro405', [], 405);
					break;

				case 500:

					//return response()->json([ 'mensagem' => 'Voltamos já' ], 500);
					return response()->view('erro500', [], 500);
					break;

				default:

					return $this->renderHttpException($e);
					break;
			}
		
		}else {

			return parent::render($request, $e);
		}
	}

}
