<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Prioridade extends Model {

	//1 prioridade tem muitas tarefas
	public function tarefas() {

		return $this->hasMany('App\Tarefa');
	}
}