<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use App\Tarefa;
use App\Prioridade;
use App\Concluida;
use App\Http\Requests\TarefaRequest;
use App\ConverterData;
use Session;
use App\Dao;
use Request;

class TarefaController extends Controller {

	private $total_tarefas_por_pagina = 2;

	public function __construct() {

		$this->middleware('autorizador');
	}

	//retorna a view do formulário
	public function novo() {

		return view('formulario')->with('prioridades', Prioridade::all())->with('concluidas', Concluida::all());
	}

	//adicionar tarefas
	public function listar(TarefaRequest $request) {

		$params = $request->all();
		$tarefas = new Tarefa($params);

		$tarefas->prazo = ConverterData::data_formato_brasileiro_para_formato_bd($tarefas->prazo);

		$tarefas->save();

		//return response()->json($tarefas, 201);

		return redirect('/tarefas/lista')->withInput();
	}

	//lista todas as tarefas
	public function visualizar() {

		$tarefas = Tarefa::paginate($this->total_tarefas_por_pagina);

		//return response()->json($tarefas);

		return view('lista')->with('tarefas', $tarefas)->with('concluidas', Concluida::all())->with('prioridades', 
			   Prioridade::all());;
	}

	//detalhes da tarefa x
	public function mostra($id) {

		$tarefa = Tarefa::find($id);

		if(!$tarefa)
		
			return response()->view('erro404', [], 404);

		//if(!$tarefa)

		//	return response()->json([ 'mensagem' => 'Tarefa não encontrada' ], 404);

		$tarefa->prazo = ConverterData::data_formato_brasileiro_para_formato_bd($tarefa->prazo);

		//return response()->json($tarefa);

		return view('detalhes')->with('tarefa', $tarefa);
	}

	//chama o formulario para editar tarefa x
	public function editar($id) {

		$tarefa = Tarefa::find($id);

		if(!$tarefa)

			return response()->view('erro404', [], 404);

		//if(!$tarefa)

		//	return response()->json([ 'mensagem' => 'Tarefa não encontrada' ], 404);

		//return response()->json($tarefa);

		return view('formulario-editar')->with('tarefa', $tarefa)->with('prioridades', 
			         Prioridade::all())->with('concluidas', Concluida::all());
	}

	//altera a tarefa x no bd
	public function altera(TarefaRequest $request, $id) {

		$tarefa = Tarefa::find($id);

		$params = $request->all();

		$params['prazo'] = ConverterData::data_formato_brasileiro_para_formato_bd($request->prazo);

		$tarefa->update($params);

		Session::flash('mensagem', "Tarefa " . $tarefa->nome . " alterada com sucesso!");

		//return response()->json($tarefa);

		return redirect('/tarefas/lista');
	}

	//deleta 1 tarefa
	public function deletar() {

		$id = Request::input('tarefa_id');
		$tarefa = Tarefa::find($id);
		$tarefa->delete();

		Session::flash('mensagem', "Tarefa " . $tarefa->nome . " deletada com sucesso!");

		return redirect('/tarefas/lista');
	}

	//filtragem e paginação
	public function pesquisa_filtrada() {

		$dados = Request::only(['concluida_id', 'prioridade_id']);

			$tarefas = Tarefa::where(function($query) use ($dados) {

					   	if(!empty($dados['concluida_id']))

							$query->where('concluida_id', '=', $dados['concluida_id']);

						if(!empty($dados['prioridade_id']))

							$query->where('prioridade_id', '=', $dados['prioridade_id']);

						})->paginate($this->total_tarefas_por_pagina);

		return view('lista')->with('tarefas', $tarefas)->with('concluidas', Concluida::all())->with(
			   'dados', $dados)->with('prioridades', Prioridade::all());
	}
}