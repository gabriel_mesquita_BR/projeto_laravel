<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class TarefaRequest extends Request {

	//autorizo as validações
	public function authorize() {

		return true;
	}

	//validações dos campos
	public function rules() {

		return [

			'nome' => 'required',
			'descricao' => 'required',
			'prazo' => 'required|date',
			'prioridade_id' => 'required',
			'concluida_id' => 'required'
		];
	}

	//mensagens de erro das validações
	public function messages() {

		return [

			'required' => ':attribute é obrigatório!',
			'date' => ':attribute não tem uma data válida!'
		];
	}
}