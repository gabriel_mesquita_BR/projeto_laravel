<?php

Route::get('/', 'IndexController@index');

Route::get('/home', 'HomeController@login');

Route::controllers([

	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController'
]);

Route::get('/tarefas/novo', 'TarefaController@novo');
Route::post('/tarefas/adiciona', 'TarefaController@listar');

Route::get('/tarefas/lista', 'TarefaController@visualizar');
Route::get('/tarefas/mostra/{id}', 'TarefaController@mostra');

Route::get('/tarefas/edita/{id}', 'TarefaController@editar');
Route::post('/tarefas/altera/{id}', 'TarefaController@altera');

Route::post('/tarefas/remove', 'TarefaController@deletar');

Route::any('/tarefas/lista-filtrada', 'TarefaController@pesquisa_filtrada');

Route::get('/logout', 'HomeController@logout');