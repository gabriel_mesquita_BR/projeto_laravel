<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Concluida extends Model {

	public function tarefa() {

		//1 conclusão pode estar relacionada a várias tarefas
		return $this->hasMany('App\Tarefa');
	}
}