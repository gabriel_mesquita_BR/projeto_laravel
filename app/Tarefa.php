<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarefa extends Model {

	protected $fillable = array('nome', 'descricao', 'prazo', 'prioridade_id', 'concluida_id');

	//1 tarefa pertence a 1 prioridade
	public function prioridade() {

		return $this->belongsTo('App\Prioridade');
	}

	//1 tarefa pode ser concluída 1 vez
	public function concluida() {

		return $this->belongsTo('App\Concluida');
	}
}